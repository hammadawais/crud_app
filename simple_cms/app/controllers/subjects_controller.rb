class SubjectsController < ApplicationController

   layout "admin"

  def index
    @subjects = Subject.sorted
  end

  def show
    @subject = Subject.find(params[:id])
  end

  def new
    @subject = Subject.new({:name => "default"})

  end
  def create
    # instantiate new object  using form parameters
    @subject = Subject.new(params_for_subject)
    # save the object
    if @subject.save
      flash[:notice] = " Subject Created Successfully"
    # if save succeeds, redirect to the index action
      redirect_to(:action => 'index')
     else
    # if save fails, redirects to the from so user can fix the problem
      render('new')
      end

  end


  def edit
    @subject = Subject.find(params[:id])
  end
  def update
    # find the existing object using form params
    @subject = Subject.find(params[:id])
    #update the object
    if @subject.update_attributes(params_for_subject)
      # if update succeeds, redirect to the index action
      flash[:notice] = "Subject  updated successfully"
      redirect_to(:action => 'show', :id => @subject.id)
    else
      # if update fails, redirects to the from so user can fix the problem
      render('edit')
    end

  end

  def delete
    @subject = Subject.find(params[:id])
  end
  def destroy

    subject = Subject.find(params[:id]).destroy
    flash[:notice] = "Subject '#{subject.name}' deleted successfully"
    redirect_to(:action => 'index')
  end

  private
    def params_for_subject
      params.require(:subject).permit(:name,:position, :visible)
    end
end
